import {
    MDCTopAppBar
} from '@material/top-app-bar'
import {
    MDCDrawer
} from '@material/drawer'
import {
    MDCTabBar
} from '@material/tab-bar'
import {
    MDCFormField
} from '@material/form-field'
import {
    MDCCheckbox
} from '@material/checkbox'
import {
    MDCMenu
} from '@material/menu';
import {
    MDCSelect
} from '@material/select';
import {
    MDCTextField
} from '@material/textfield';


const $ = require('jquery')

const book_types = []

$(() => {
    // Navbar
    const topAppBar = new MDCTopAppBar($('#app-bar')[0])
    topAppBar.setScrollTarget($('#main-content')[0])
    topAppBar.listen('MDCTopAppBar:nav', () => drawer.open = !drawer.open)

    // Drawer
    const drawer = new MDCDrawer($('.mdc-drawer')[0])
    drawer.open = true

    // Mode tab bar
    const mode = new MDCTabBar($('#mode')[0])

    // Auto load image checkbox
    const auto_load_image__form_field = new MDCFormField($('#auto-load-image')[0])
    auto_load_image__form_field.input = new MDCCheckbox($('#auto-load-image>.mdc-checkbox')[0])

    // Counties select
    const counties__menu = new MDCMenu($('#counties__menu')[0])
    counties__menu.hoistMenuToBody()
    counties__menu.setAbsolutePosition($('#counties__select').position().left, $('#counties__select').position().top + 40)

    $('#counties__select').click((e) => counties__menu.open = !counties__menu.open)

    $.get('assets/counties.json', (counties) => {
        counties.forEach(county => {
            let countyEl = $('.county:first').clone()
            $(countyEl).attr('data-value', county.replace(/\s+/g, ''))
            $(countyEl).find('.label').text(county)

            $('.counties').append(countyEl)
        })
        $('li.county').click((e) => {
            let input = $(e.target).closest('li').find('input')[0]
            e.stopPropagation()
            if (!$(e.target).is('input')) {
                input.checked = !input.checked
            }

            if ($(e.target).closest('li').attr('data-value') == 'all') {
                $('.county:not(:first)').each((i, el) => {
                    $(el).find('input')[0].checked = input.checked
                })
            } else {
                let checked = 0
                $('.county:not(:first)').each((i, el) => {
                    !$(el).find('input')[0].checked ?
                        $('.county:first').find('input')[0].checked = false :
                        checked++
                })

                if ($(counties).length == checked)
                    $('.county:first').find('input')[0].checked = true
            }
            $('#counties__select>span').text(`${$('.county:not(:first)>div>input:checked').length} selected`)
        })
    })



    // Book types select
    const book_types__select = new MDCSelect($('#book_types__select')[0])
    $.get('assets/book_types.json', (book_types) => {
        book_types.forEach(book_type => {
            $('<li/>', {
                class: 'mdc-list-item',
                'data-value': book_type.replace(/\s+/g, ''),
                role: 'option'
            }).text(book_type).appendTo($('#book_types__menu>ul'))
        })
    })

    // Date range
    const start_date = new MDCTextField($('#start-date')[0])
    const end_date = new MDCTextField($('#end-date')[0])

    // Search
    const search = new MDCTextField($('#search')[0])
    $('#exact-search__toggle').click((e) => {
        $(e.target).toggleClass('enabled')

        $('#search__input').attr('placeholder',
            `Exact search is ${$(e.target).hasClass('enabled') ? 'on' : 'off'}`
        )
    })

    $('#search__button').click(() => {

    })
})