import $ from 'jquery'

import {
    MDCFormField
} from '@material/form-field'
import {
    MDCCheckbox
} from '@material/checkbox'
import {
    MDCMenu
} from '@material/menu'

import checkbox_component from './components/checkbox.html'
import counties_dropdown_component from './components/counties.dropdown.html'


$(() => {
    $('body').addClass('body')

    $('#btnLogout, #btnCapture1').addClass('navbar__button')

    $('ul.rtsUL').addClass('tabs')

    $('.RadListBox, .driHide').remove()

    $('#upCountiesPN').nextAll('br').remove()

    // Generate checkbox for auto load image
    $('#cbxLoadImageAll').before(generateCheckbox('cbxLoadImageAll', 'Auto Load Image'))
    $('#cbxLoadImageAll, label[for="cbxLoadImageAll"]').hide()

    // Generate checkbox for party name search exact
    $('#cbxPNSearchType').before(generateCheckbox('cbxPNSearchType', 'Search exact'))
    $('#cbxPNSearchType').hide()
})

function generateCheckbox(id, label) {
    let $checkbox_component = $(checkbox_component).clone()

    $checkbox_component.find('input').attr('id', id + '--themed').attr('checked', $('#' + id).attr('checked'))
    $checkbox_component.find('label').attr('for', id + '--themed').text(label).css('margin-bottom', 0)

    $checkbox_component.find('input').change(() => {
        $('#' + id).trigger('click')
    })
    return $checkbox_component
}